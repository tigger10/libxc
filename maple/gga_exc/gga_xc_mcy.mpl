(*
 Copyright (C) 2018 M.A.L. Marques

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
*)

(* type: gga_exc *)
(* prefix:
  gga_xc_mcy_params *params;

  assert(p->params != NULL);
  params = (gga_xc_mcy_params * )(p->params);
*)

$define gga_x_b88_params
$include "gga_x_b88.mpl"
$include "gga_c_lyp.mpl"

#scaled LYP correlation

new_t45 := (rs, z, xt, xs0, xs1) -> 
 + (params_a_c + params_a_d/(rs*params_a_d + 1)^2) * (1-z^2)
      * (xt^2 * (7/72) + aux5*z*(xs0^2*(1 + z)^(8/3) - xs1^2*(1 - z)^(8/3))):

#t1 scaled by lambda
#omega scaled by lambda
lyp_comp := (rs, z, xt, xs0, xs1) -> 
  + params_a_A * rs *
    ( params_a_c * t1(params_a_lambda_p*rs,z)
      + omega(params_a_lambda_p*rs) * new_t45(params_a_lambda_p*rs, z, xt, xs0, xs1)):

#rs = rs'
der_prefix := rs -> 
  + rs * params_a_c
  + (rs * params_a_d + (1/params_a_lambda_p))
        / (rs*params_a_lambda_p*params_a_d + 1)^2:

lyp_lambda := (rs, z, xt, xs0, xs1) -> 
  + f_lyp(rs*params_a_lambda_p, z, xt, xs0, xs1):

lyp_der := (rs, z, xt, xs0, xs1) -> 
  + der_prefix(rs/RS_FACTOR) * lyp_lambda(rs, z, xt, xs0, xs1)
  + lyp_comp(rs/RS_FACTOR, z, xt, xs0, xs1):

f:= (rs, z, xt, xs0, xs1) ->
   + params_a_scale_lambda_p * (
    + gga_exchange(b88_f, rs, zeta, xs0, xs1)
    + params_a_lambda_p * lyp_lambda(rs, z, xt, xs0, xs1)
    + params_a_lambda_p * params_a_lambda_p * lyp_der(rs, z, xt, xs0, xs1)
   ):

