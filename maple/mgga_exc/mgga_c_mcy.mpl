(*
 Copyright (C) 2019 Jaewook Kim

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
*)

(* type: mgga_exc *)
(* prefix:
  mgga_c_mcy_params *params;

  assert(pt->params != NULL);
  params = (mgga_c_mcy_params * )(pt->params);
*)


(* from MCY 
#params_a_lambda_0 := 0.0000000001:

#from mgga_c_tpss.c
#params_a_d    := 2.8:
#params_a_C0_c := [0.53, 0.87, 0.50, 2.26]:


#$include "tpss.mpl"
*)
$define gga_c_pbe_params
$include "gga_c_pbe.mpl"

(* csi2 and C0 are different with tpss.*)

mcy_grad_zeta2 := (rs, z, xt, xs0, xs1) ->
  ((n_total(rs))^(2/3)) * (1 - z^2) * (t_total(z, xs0^2, xs1^2) - xt^2):

(*  the factor lambda^2 is already encountered in mcy_grad_zeta2 *)

tpss_C00 := (cc, z) ->
  + add(cc[i]*z^(2*(i-1)), i=1..4):

mcy_C0 := (cc, rs, z, xt, xs0, xs1) ->
  + tpss_C00(cc, z)
  / (1 + mcy_grad_zeta2(rs, z, xt, xs0, xs1) * ((1 + z)^(4/3) + (1 - z)^(4/3) -2)/2)^4:

(* Equation 11, with tau_W from Equation 12 *)
tpss_aux := (z, xt, ts0, ts1) ->
  +  m_min(xt^2/(8*t_total(z, ts0, ts1)), 1):

mcy_perp := (f_gga, rs, z, xt, xs0, xs1, ts0, ts1) ->
  + (1 + mcy_C0(params_a_C0_c, rs, z, xt, xs0, xs1)*tpss_aux(z, xt, ts0, ts1)^2)
  * f_gga(rs, z, xt, xs0, xs1):

mcy_par  := (f_gga, rs, z, xt, xs0, xs1, ts0, ts1) ->
  - (1 + mcy_C0(params_a_C0_c, rs, z, xt, xs0, xs1))
      * tpss_aux(z, xt, ts0, ts1)^2
      * ( + m_max(f_gga(rs*(2/(1 + z))^(1/3),  1, xs0, xs0, 0), f_gga(rs, z, xt, xs0, xs1))*(1 + z)/2
          + m_max(f_gga(rs*(2/(1 - z))^(1/3), -1, xs1, 0, xs1), f_gga(rs, z, xt, xs0, xs1))*(1 - z)/2
        ):

mcy0 := (f_gga, rs, z, xt, xs0, xs1, ts0, ts1) ->
  + mcy_par (f_gga, rs, z, xt, xs0, xs1, ts0, ts1)
  + mcy_perp(f_gga, rs, z, xt, xs0, xs1, ts0, ts1):

(* # 2 is mutiplied in front of the integration*)
f_mcyb := (f_gga, rs, z, xt, xs0, xs1, ts0, ts1) ->
  + 2 * mcy0(f_gga, rs*params_a_lambda_0, z, xt, xs0, xs1, ts0, ts1)
  * (1 + params_a_d * mcy0(f_gga, rs*params_a_lambda_0, z, xt, xs0, xs1, ts0, ts1)
           * tpss_aux(z, xt, ts0, ts1)^3
    ):

f:= (rs, z, xt, xs0, xs1, us0, us1, ts0, ts1) ->
   + params_a_scale_b
   * f_mcyb(f_pbe, rs, z, xt, xs0, xs1, ts0, ts1):

