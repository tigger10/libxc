/* 
  This file was generated automatically with ./scripts/maple2c.pl.
  Do not edit this file directly as it can be overwritten!!

  This Source Code Form is subject to the terms of the Mozilla Public
  License, v. 2.0. If a copy of the MPL was not distributed with this
  file, You can obtain one at http://mozilla.org/MPL/2.0/.

  Maple version     : Maple 2018 (X86 64 LINUX)
  Maple source      : ./maple/gga_exc/gga_xc_mcy.mpl
  Type of functional: gga_exc
*/

#define maple2c_order 1
#define MAPLE2C_FLAGS (XC_FLAGS_I_HAVE_EXC | XC_FLAGS_I_HAVE_VXC)


static inline void
func_unpol(const xc_func_type *p, int order, const double *rho, const double *sigma, double *zk, GGA_OUT_PARAMS_NO_EXC(double *))
{

#ifndef XC_DONT_COMPILE_EXC
  double t7, t9, t11, t12, t13, t15, t16, t17;
  double t18, t19, t20, t22, t23, t24, t25, t26;
  double t27, t28, t30, t31, t32, t34, t36, t37;
  double t40, t41, t46, t51, t52, t53, t54, t58;
  double t59, t60, t62, t63, t66, t67, t68, t71;
  double t72, t74, t76, t77, t78, t82, t83, t86;
  double t87, t91, t92, t94, t96, t100, t101, t102;
  double t105, t107, t109, t110, t111, t112, t116;

#ifndef XC_DONT_COMPILE_VXC
  double t121, t122, t127, t129, t135, t136, t137, t138;
  double t146, t147, t156, t167, t170, t181, t184, t191;
  double t211, t221, t234, t235, t238, t244, t248, t278;
  double t293;
#endif

#endif


  gga_xc_mcy_params *params;

  assert(p->params != NULL);
  params = (gga_xc_mcy_params * )(p->params);

  t7 = M_CBRT3;
  t9 = POW_1_3(0.1e1 / M_PI);
  t11 = M_CBRT4;
  t12 = t11 * t11;
  t13 = t7 * t9 * t12;
  t15 = 0.1e1 / 0.2e1 + zeta / 0.2e1;
  t16 = POW_1_3(t15);
  t17 = t16 * t15;
  t18 = POW_1_3(rho[0]);
  t19 = t17 * t18;
  t20 = t7 * t7;
  t22 = t20 / t9;
  t23 = t22 * t11;
  t24 = M_CBRT2;
  t25 = t24 * t24;
  t26 = sigma[0] * t25;
  t27 = rho[0] * rho[0];
  t28 = t18 * t18;
  t30 = 0.1e1 / t28 / t27;
  t31 = sqrt(sigma[0]);
  t32 = t31 * t24;
  t34 = 0.1e1 / t18 / rho[0];
  t36 = log(t32 * t34 + sqrt(POW_2(t32 * t34) + 0.1e1));
  t37 = t34 * t36;
  t40 = 0.10e1 + 0.2520e-1 * t32 * t37;
  t41 = 0.1e1 / t40;
  t46 = 0.10e1 + 0.93333333333333333332e-3 * t23 * t26 * t30 * t41;
  t51 = 0.1e1 / 0.2e1 - zeta / 0.2e1;
  t52 = POW_1_3(t51);
  t53 = t52 * t51;
  t54 = t53 * t18;
  t58 = params->lambda_p * params->A;
  t59 = params->d * params->lambda_p;
  t60 = 0.1e1 / t18;
  t62 = t59 * t60 + 0.1e1;
  t63 = 0.1e1 / t62;
  t66 = exp(-params->c * params->lambda_p * t60);
  t67 = params->B * t66;
  t68 = sigma[0] * t30;
  t71 = (params->d * t63 + params->c) * params->lambda_p;
  t72 = t71 * t60;
  t74 = -0.1e1 / 0.72e2 - 0.7e1 / 0.72e2 * t72;
  t76 = M_PI * M_PI;
  t77 = POW_1_3(t76);
  t78 = t77 * t77;
  t82 = 0.5e1 / 0.2e1 - t72 / 0.18e2;
  t83 = t82 * sigma[0];
  t86 = t72 - 0.11e2;
  t87 = t86 * sigma[0];
  t91 = -t68 * t74 - 0.3e1 / 0.10e2 * t20 * t78 + t83 * t30 / 0.8e1 + t87 * t30 / 0.144e3 - 0.5e1 / 0.24e2 * t68;
  t92 = t63 * t91;
  t94 = t67 * t92 - t63;
  t96 = params->lambda_p * params->lambda_p;
  t100 = params->d * t60 + 0.1e1 / params->lambda_p;
  t101 = t62 * t62;
  t102 = 0.1e1 / t101;
  t105 = (t100 * t102 + params->c * t60) * params->A;
  t107 = params->A * t60;
  t109 = t67 * t63;
  t110 = params->d * t102;
  t111 = params->c + t110;
  t112 = t111 * sigma[0];
  t116 = -params->c * t63 + 0.7e1 / 0.72e2 * t109 * t112 * t30;
  if(zk != NULL && (p->info->flags & XC_FLAGS_HAVE_EXC))
    zk[0] = params->scale_lambda_p * (-0.3e1 / 0.8e1 * t13 * t19 * t46 - 0.3e1 / 0.8e1 * t13 * t54 * t46 + t58 * t94 + t96 * (t105 * t94 + t107 * t116));

#ifndef XC_DONT_COMPILE_VXC

  if(order < 1) return;


  t121 = rho[0] * params->scale_lambda_p;
  t122 = 0.1e1 / t28;
  t127 = t27 * rho[0];
  t129 = 0.1e1 / t28 / t127;
  t135 = t22 * t11 * sigma[0];
  t136 = t25 * t30;
  t137 = t40 * t40;
  t138 = 0.1e1 / t137;
  t146 = sqrt(t26 * t30 + 0.1e1);
  t147 = 0.1e1 / t146;
  t156 = -0.24888888888888888889e-2 * t23 * t26 * t129 * t41 - 0.93333333333333333332e-3 * t135 * t136 * t138 * (-0.33600000000000000000e-1 * t32 / t18 / t27 * t36 - 0.33600000000000000000e-1 * t26 * t129 * t147);
  t167 = params->lambda_p * t34;
  t170 = params->B * params->c;
  t181 = sigma[0] * t129;
  t184 = params->d * params->d;
  t191 = -t184 * t102 * t96 / t28 / rho[0] + t71 * t34;
  t211 = -t110 * t167 / 0.3e1 + t170 * params->lambda_p * t34 * t66 * t92 / 0.3e1 + t67 * t102 * t91 * params->d * t167 / 0.3e1 + t67 * t63 * (0.8e1 / 0.3e1 * t181 * t74 - 0.7e1 / 0.216e3 * t68 * t191 - t83 * t129 / 0.3e1 - t87 * t129 / 0.54e2 + 0.5e1 / 0.9e1 * t181);
  t221 = t59 * t34;
  t234 = t27 * t27;
  t235 = 0.1e1 / t234;
  t238 = t66 * t63;
  t244 = sigma[0] * t235;
  t248 = t101 * t101;
  if(vrho != NULL && (p->info->flags & XC_FLAGS_HAVE_VXC))
    vrho[0] = (params->scale_lambda_p * (-0.3e1 / 0.8e1 * t13 * t19 * t46 - 0.3e1 / 0.8e1 * t13 * t54 * t46 + t58 * t94 + t96 * (t105 * t94 + t107 * t116))) + t121 * (-t13 * t17 * t122 * t46 / 0.8e1 - 0.3e1 / 0.8e1 * t13 * t19 * t156 - t13 * t53 * t122 * t46 / 0.8e1 - 0.3e1 / 0.8e1 * t13 * t54 * t156 + t58 * t211 + t96 * ((-params->c * t34 / 0.3e1 - params->d * t34 * t102 / 0.3e1 + 0.2e1 / 0.3e1 * t100 / t101 / t62 * t221) * params->A * t94 + t105 * t211 - params->A * t34 * t116 / 0.3e1 + t107 * (-params->c * t102 * t221 / 0.3e1 + 0.7e1 / 0.216e3 * t170 * params->lambda_p * t235 * t238 * t112 + 0.7e1 / 0.216e3 * t67 * t102 * t111 * t244 * t59 + 0.7e1 / 0.108e3 * t67 / t248 * t184 * params->lambda_p * t244 - 0.7e1 / 0.27e2 * t109 * t112 * t129)));

  t278 = 0.93333333333333333332e-3 * t23 * t136 * t41 - 0.93333333333333333332e-3 * t135 * t136 * t138 * (0.12600000000000000000e-1 / t31 * t24 * t37 + 0.12600000000000000000e-1 * t136 * t147);
  t293 = t238 * (-t30 * t74 + t82 * t30 / 0.8e1 + t86 * t30 / 0.144e3 - 0.5e1 / 0.24e2 * t30);
  if(vrho != NULL && (p->info->flags & XC_FLAGS_HAVE_VXC))
    vsigma[0] = t121 * (-0.3e1 / 0.8e1 * t13 * t19 * t278 - 0.3e1 / 0.8e1 * t13 * t54 * t278 + t58 * params->B * t293 + t96 * (t105 * params->B * t293 + 0.7e1 / 0.72e2 * params->A / t127 * params->B * t238 * t111));

#ifndef XC_DONT_COMPILE_FXC

  if(order < 2) return;


#endif

#endif


}


static inline void
func_ferr(const xc_func_type *p, int order, const double *rho, const double *sigma, double *zk, GGA_OUT_PARAMS_NO_EXC(double *))
{

#ifndef XC_DONT_COMPILE_EXC
  double t7, t10, t12, t13, t14, t16, t18, t19;
  double t20, t21, t23, t24, t25, t26, t27, t28;
  double t29, t32, t33, t39;

#ifndef XC_DONT_COMPILE_VXC
  double t43, t44, t48, t52, t53, t60, t61;
#endif

#endif


  gga_xc_mcy_params *params;

  assert(p->params != NULL);
  params = (gga_xc_mcy_params * )(p->params);

  t7 = M_CBRT3;
  t10 = POW_1_3(0.1e1 / M_PI);
  t12 = M_CBRT4;
  t13 = t12 * t12;
  t14 = POW_1_3(rho[0]);
  t16 = t7 * t7;
  t18 = t16 / t10;
  t19 = t18 * t12;
  t20 = rho[0] * rho[0];
  t21 = t14 * t14;
  t23 = 0.1e1 / t21 / t20;
  t24 = sigma[0] * t23;
  t25 = sqrt(sigma[0]);
  t26 = t14 * rho[0];
  t27 = 0.1e1 / t26;
  t28 = t25 * t27;
  t29 = log(t28 + sqrt(t28 * t28 + 0.1e1));
  t32 = 0.10e1 + 0.2520e-1 * t28 * t29;
  t33 = 0.1e1 / t32;
  t39 = params->scale_lambda_p * t7 * t10 * t13 * t14 * (0.10e1 + 0.93333333333333333332e-3 * t19 * t24 * t33);
  if(zk != NULL && (p->info->flags & XC_FLAGS_HAVE_EXC))
    zk[0] = -0.3e1 / 0.8e1 * t39;

#ifndef XC_DONT_COMPILE_VXC

  if(order < 1) return;


  t43 = t26 * params->scale_lambda_p * t7;
  t44 = t10 * t13;
  t48 = sigma[0] / t21 / t20 / rho[0];
  t52 = t32 * t32;
  t53 = 0.1e1 / t52;
  t60 = sqrt(t24 + 0.1e1);
  t61 = 0.1e1 / t60;
  if(vrho != NULL && (p->info->flags & XC_FLAGS_HAVE_VXC))
    vrho[0] = -t39 / 0.2e1 - 0.3e1 / 0.8e1 * t43 * t44 * (-0.24888888888888888889e-2 * t19 * t48 * t33 - 0.93333333333333333332e-3 * t19 * t24 * t53 * (-0.33600000000000000000e-1 * t25 / t14 / t20 * t29 - 0.33600000000000000000e-1 * t48 * t61));

  if(vrho != NULL && (p->info->flags & XC_FLAGS_HAVE_VXC))
    vsigma[0] = -0.3e1 / 0.8e1 * t43 * t44 * (0.93333333333333333332e-3 * t18 * t12 * t23 * t33 - 0.93333333333333333332e-3 * t19 * t24 * t53 * (0.12600000000000000000e-1 / t25 * t27 * t29 + 0.12600000000000000000e-1 * t23 * t61));

#ifndef XC_DONT_COMPILE_FXC

  if(order < 2) return;


#endif

#endif


}


static inline void
func_pol(const xc_func_type *p, int order, const double *rho, const double *sigma, double *zk, GGA_OUT_PARAMS_NO_EXC(double *))
{

#ifndef XC_DONT_COMPILE_EXC
  double t7, t9, t11, t12, t13, t15, t16, t17;
  double t18, t19, t20, t21, t23, t24, t25, t26;
  double t27, t29, t30, t31, t33, t34, t35, t38;
  double t39, t43, t48, t49, t50, t51, t52, t53;
  double t54, t56, t57, t58, t60, t61, t62, t65;
  double t66, t70, t74, t75, t76, t77, t78, t80;
  double t81, t82, t84, t85, t89, t90, t92, t93;
  double t95, t96, t99, t100, t102, t105, t107, t108;
  double t109, t110, t111, t112, t113, t114, t115, t116;
  double t117, t118, t119, t120, t121, t122, t123, t127;
  double t128, t130, t131, t132, t133, t134, t137, t139;
  double t142, t144, t145, t150, t151, t154, t155, t161;
  double t162, t164, t166, t170, t171, t172, t175, t177;
  double t178, t180, t182, t183, t185, t186, t190, t191;
  double t193;

#ifndef XC_DONT_COMPILE_VXC
  double t198, t199, t203, t206, t207, t211, t212, t219;
  double t220, t235, t236, t237, t238, t239, t241, t245;
  double t246, t248, t249, t254, t257, t260, t263, t265;
  double t267, t274, t276, t283, t284, t285, t286, t287;
  double t288, t294, t300, t301, t302, t303, t315, t331;
  double t335, t339, t346, t353, t356, t370, t374, t379;
  double t381, t384, t387, t390, t391, t397, t401, t404;
  double t407, t424, t425, t429, t430, t437, t438, t451;
  double t460, t461, t462, t463, t469, t472, t473, t474;
  double t515, t518, t559, t560, t577, t579, t581, t582;
  double t583, t595, t643;
#endif

#endif


  gga_xc_mcy_params *params;

  assert(p->params != NULL);
  params = (gga_xc_mcy_params * )(p->params);

  t7 = M_CBRT3;
  t9 = POW_1_3(0.1e1 / M_PI);
  t11 = M_CBRT4;
  t12 = t11 * t11;
  t13 = t7 * t9 * t12;
  t15 = 0.1e1 / 0.2e1 + zeta / 0.2e1;
  t16 = POW_1_3(t15);
  t17 = t16 * t15;
  t18 = rho[0] + rho[1];
  t19 = POW_1_3(t18);
  t20 = t17 * t19;
  t21 = t7 * t7;
  t23 = t21 / t9;
  t24 = t23 * t11;
  t25 = rho[0] * rho[0];
  t26 = POW_1_3(rho[0]);
  t27 = t26 * t26;
  t29 = 0.1e1 / t27 / t25;
  t30 = sigma[0] * t29;
  t31 = sqrt(sigma[0]);
  t33 = 0.1e1 / t26 / rho[0];
  t34 = t31 * t33;
  t35 = log(t34 + sqrt(t34 * t34 + 0.1e1));
  t38 = 0.10e1 + 0.2520e-1 * t34 * t35;
  t39 = 0.1e1 / t38;
  t43 = 0.10e1 + 0.93333333333333333332e-3 * t24 * t30 * t39;
  t48 = 0.1e1 / 0.2e1 - zeta / 0.2e1;
  t49 = POW_1_3(t48);
  t50 = t49 * t48;
  t51 = t50 * t19;
  t52 = rho[1] * rho[1];
  t53 = POW_1_3(rho[1]);
  t54 = t53 * t53;
  t56 = 0.1e1 / t54 / t52;
  t57 = sigma[2] * t56;
  t58 = sqrt(sigma[2]);
  t60 = 0.1e1 / t53 / rho[1];
  t61 = t58 * t60;
  t62 = log(t61 + sqrt(t61 * t61 + 0.1e1));
  t65 = 0.10e1 + 0.2520e-1 * t61 * t62;
  t66 = 0.1e1 / t65;
  t70 = 0.10e1 + 0.93333333333333333332e-3 * t24 * t57 * t66;
  t74 = params->lambda_p * params->A;
  t75 = rho[0] - rho[1];
  t76 = t75 * t75;
  t77 = t18 * t18;
  t78 = 0.1e1 / t77;
  t80 = -t76 * t78 + 0.1e1;
  t81 = params->d * params->lambda_p;
  t82 = 0.1e1 / t19;
  t84 = t81 * t82 + 0.1e1;
  t85 = 0.1e1 / t84;
  t89 = exp(-params->c * params->lambda_p * t82);
  t90 = params->B * t89;
  t92 = sigma[0] + 0.2e1 * sigma[1] + sigma[2];
  t93 = t19 * t19;
  t95 = 0.1e1 / t93 / t77;
  t96 = t92 * t95;
  t99 = (params->d * t85 + params->c) * params->lambda_p;
  t100 = t99 * t82;
  t102 = 0.47e2 - 0.7e1 * t100;
  t105 = t80 * t102 / 0.72e2 - 0.2e1 / 0.3e1;
  t107 = M_PI * M_PI;
  t108 = POW_1_3(t107);
  t109 = t108 * t108;
  t110 = t21 * t109;
  t111 = 0.1e1 / t18;
  t112 = t75 * t111;
  t113 = 0.1e1 + t112;
  t114 = t113 * t113;
  t115 = POW_1_3(t113);
  t116 = t115 * t115;
  t117 = t116 * t114;
  t118 = 0.1e1 - t112;
  t119 = t118 * t118;
  t120 = POW_1_3(t118);
  t121 = t120 * t120;
  t122 = t121 * t119;
  t123 = t117 + t122;
  t127 = M_CBRT2;
  t128 = t127 * t80;
  t130 = 0.5e1 / 0.2e1 - t100 / 0.18e2;
  t131 = t30 * t117;
  t132 = t57 * t122;
  t133 = t131 + t132;
  t134 = t130 * t133;
  t137 = t100 - 0.11e2;
  t139 = t116 * t114 * t113;
  t142 = t121 * t119 * t118;
  t144 = t30 * t139 + t57 * t142;
  t145 = t137 * t144;
  t150 = t114 * sigma[2];
  t151 = t56 * t122;
  t154 = t119 * sigma[0];
  t155 = t29 * t117;
  t161 = -t96 * t105 - 0.3e1 / 0.20e2 * t110 * t80 * t123 + t128 * t134 / 0.32e2 + t128 * t145 / 0.576e3 - t127 * (0.2e1 / 0.3e1 * t131 + 0.2e1 / 0.3e1 * t132 - t150 * t151 / 0.4e1 - t154 * t155 / 0.4e1) / 0.8e1;
  t162 = t85 * t161;
  t164 = t90 * t162 - t80 * t85;
  t166 = params->lambda_p * params->lambda_p;
  t170 = params->d * t82 + 0.1e1 / params->lambda_p;
  t171 = t84 * t84;
  t172 = 0.1e1 / t171;
  t175 = (t170 * t172 + params->c * t82) * params->A;
  t177 = params->A * t82;
  t178 = params->c * t80;
  t180 = t90 * t85;
  t182 = params->d * t172 + params->c;
  t183 = t182 * t80;
  t185 = t127 * t75;
  t186 = t131 - t132;
  t190 = 0.7e1 / 0.72e2 * t96 + t185 * t111 * t186 / 0.576e3;
  t191 = t183 * t190;
  t193 = -t178 * t85 + t180 * t191;
  if(zk != NULL && (p->info->flags & XC_FLAGS_HAVE_EXC))
    zk[0] = params->scale_lambda_p * (-0.3e1 / 0.8e1 * t13 * t20 * t43 - 0.3e1 / 0.8e1 * t13 * t51 * t70 + t74 * t164 + t166 * (t175 * t164 + t177 * t193));

#ifndef XC_DONT_COMPILE_VXC

  if(order < 1) return;


  t198 = t18 * params->scale_lambda_p;
  t199 = 0.1e1 / t93;
  t203 = t13 * t17 * t199 * t43 / 0.8e1;
  t206 = 0.1e1 / t27 / t25 / rho[0];
  t207 = sigma[0] * t206;
  t211 = t38 * t38;
  t212 = 0.1e1 / t211;
  t219 = sqrt(t30 + 0.1e1);
  t220 = 0.1e1 / t219;
  t235 = t13 * t50 * t199 * t70 / 0.8e1;
  t236 = t75 * t78;
  t237 = t77 * t18;
  t238 = 0.1e1 / t237;
  t239 = t76 * t238;
  t241 = -0.2e1 * t236 + 0.2e1 * t239;
  t245 = 0.1e1 / t19 / t18;
  t246 = t81 * t245;
  t248 = t80 * t172 * t246 / 0.3e1;
  t249 = params->B * params->c;
  t254 = t249 * params->lambda_p * t245 * t89 * t162 / 0.3e1;
  t257 = params->lambda_p * t245;
  t260 = t90 * t172 * t161 * params->d * t257 / 0.3e1;
  t263 = t92 / t93 / t237;
  t265 = 0.8e1 / 0.3e1 * t263 * t105;
  t267 = params->d * params->d;
  t274 = -t267 * t172 * t166 / t93 / t18 + t99 * t245;
  t276 = 0.7e1 / 0.3e1 * t80 * t274;
  t283 = t116 * t113;
  t284 = t111 - t236;
  t285 = t283 * t284;
  t286 = t121 * t118;
  t287 = -t284;
  t288 = t286 * t287;
  t294 = t127 * t241;
  t300 = t128 * t274 * t133 / 0.1728e4;
  t301 = t207 * t117;
  t302 = t30 * t285;
  t303 = t57 * t288;
  t315 = -t128 * t274 * t144 / 0.1728e4;
  t331 = t113 * sigma[2];
  t335 = t56 * t286;
  t339 = t118 * sigma[0];
  t346 = t29 * t283;
  t353 = t265 - t96 * (t241 * t102 / 0.72e2 + t276 / 0.72e2) - 0.3e1 / 0.20e2 * t110 * t241 * t123 - 0.3e1 / 0.20e2 * t110 * t80 * (0.8e1 / 0.3e1 * t285 + 0.8e1 / 0.3e1 * t288) + t294 * t134 / 0.32e2 + t300 + t128 * t130 * (-0.8e1 / 0.3e1 * t301 + 0.8e1 / 0.3e1 * t302 + 0.8e1 / 0.3e1 * t303) / 0.32e2 + t294 * t145 / 0.576e3 + t315 + t128 * t137 * (-0.8e1 / 0.3e1 * t207 * t139 + 0.11e2 / 0.3e1 * t30 * t117 * t284 + 0.11e2 / 0.3e1 * t57 * t122 * t287) / 0.576e3 - t127 * (-0.16e2 / 0.9e1 * t301 + 0.16e2 / 0.9e1 * t302 + 0.16e2 / 0.9e1 * t303 - t331 * t151 * t284 / 0.2e1 - 0.2e1 / 0.3e1 * t150 * t335 * t287 - t339 * t155 * t287 / 0.2e1 + 0.2e1 / 0.3e1 * t154 * t206 * t117 - 0.2e1 / 0.3e1 * t154 * t346 * t284) / 0.8e1;
  t356 = t90 * t85 * t353 - t241 * t85 - t248 + t254 + t260;
  t370 = (-params->c * t245 / 0.3e1 - params->d * t245 * t172 / 0.3e1 + 0.2e1 / 0.3e1 * t170 / t171 / t84 * t246) * params->A * t164;
  t374 = params->A * t245 * t193 / 0.3e1;
  t379 = t178 * t172 * t246 / 0.3e1;
  t381 = t89 * t85;
  t384 = t249 * t257 * t381 * t191 / 0.3e1;
  t387 = t80 * t190;
  t390 = t90 * t172 * t182 * t387 * t246 / 0.3e1;
  t391 = t171 * t171;
  t397 = 0.2e1 / 0.3e1 * t90 / t391 * t267 * t257 * t387;
  t401 = 0.7e1 / 0.27e2 * t263;
  t404 = t127 * t111 * t186 / 0.576e3;
  t407 = t185 * t78 * t186 / 0.576e3;
  if(vrho != NULL && (p->info->flags & XC_FLAGS_HAVE_VXC))
    vrho[0] = (params->scale_lambda_p * (-0.3e1 / 0.8e1 * t13 * t20 * t43 - 0.3e1 / 0.8e1 * t13 * t51 * t70 + t74 * t164 + t166 * (t175 * t164 + t177 * t193))) + t198 * (-t203 - 0.3e1 / 0.8e1 * t13 * t20 * (-0.24888888888888888889e-2 * t24 * t207 * t39 - 0.93333333333333333332e-3 * t24 * t30 * t212 * (-0.33600000000000000000e-1 * t31 / t26 / t25 * t35 - 0.33600000000000000000e-1 * t207 * t220)) - t235 + t74 * t356 + t166 * (t370 + t175 * t356 - t374 + t177 * (-params->c * t241 * t85 - t379 + t384 + t390 + t397 + t180 * t182 * t241 * t190 + t180 * t183 * (-t401 + t404 - t407 + t185 * t111 * (-0.8e1 / 0.3e1 * t301 + 0.8e1 / 0.3e1 * t302 - 0.8e1 / 0.3e1 * t303) / 0.576e3))));

  t424 = 0.1e1 / t54 / t52 / rho[1];
  t425 = sigma[2] * t424;
  t429 = t65 * t65;
  t430 = 0.1e1 / t429;
  t437 = sqrt(t57 + 0.1e1);
  t438 = 0.1e1 / t437;
  t451 = 0.2e1 * t236 + 0.2e1 * t239;
  t460 = -t111 - t236;
  t461 = t283 * t460;
  t462 = -t460;
  t463 = t286 * t462;
  t469 = t127 * t451;
  t472 = t30 * t461;
  t473 = t425 * t122;
  t474 = t57 * t463;
  t515 = t265 - t96 * (t451 * t102 / 0.72e2 + t276 / 0.72e2) - 0.3e1 / 0.20e2 * t110 * t451 * t123 - 0.3e1 / 0.20e2 * t110 * t80 * (0.8e1 / 0.3e1 * t461 + 0.8e1 / 0.3e1 * t463) + t469 * t134 / 0.32e2 + t300 + t128 * t130 * (0.8e1 / 0.3e1 * t472 - 0.8e1 / 0.3e1 * t473 + 0.8e1 / 0.3e1 * t474) / 0.32e2 + t469 * t145 / 0.576e3 + t315 + t128 * t137 * (0.11e2 / 0.3e1 * t30 * t117 * t460 - 0.8e1 / 0.3e1 * t425 * t142 + 0.11e2 / 0.3e1 * t57 * t122 * t462) / 0.576e3 - t127 * (0.16e2 / 0.9e1 * t472 - 0.16e2 / 0.9e1 * t473 + 0.16e2 / 0.9e1 * t474 - t331 * t151 * t460 / 0.2e1 + 0.2e1 / 0.3e1 * t150 * t424 * t122 - 0.2e1 / 0.3e1 * t150 * t335 * t462 - t339 * t155 * t462 / 0.2e1 - 0.2e1 / 0.3e1 * t154 * t346 * t460) / 0.8e1;
  t518 = t90 * t85 * t515 - t451 * t85 - t248 + t254 + t260;
  if(vrho != NULL && (p->info->flags & XC_FLAGS_HAVE_VXC))
    vrho[1] = (params->scale_lambda_p * (-0.3e1 / 0.8e1 * t13 * t20 * t43 - 0.3e1 / 0.8e1 * t13 * t51 * t70 + t74 * t164 + t166 * (t175 * t164 + t177 * t193))) + t198 * (-t203 - t235 - 0.3e1 / 0.8e1 * t13 * t51 * (-0.24888888888888888889e-2 * t24 * t425 * t66 - 0.93333333333333333332e-3 * t24 * t57 * t430 * (-0.33600000000000000000e-1 * t58 / t53 / t52 * t62 - 0.33600000000000000000e-1 * t425 * t438)) + t74 * t518 + t166 * (t370 + t175 * t518 - t374 + t177 * (-params->c * t451 * t85 - t379 + t384 + t390 + t397 + t180 * t182 * t451 * t190 + t180 * t183 * (-t401 - t404 - t407 + t185 * t111 * (0.8e1 / 0.3e1 * t472 + 0.8e1 / 0.3e1 * t473 - 0.8e1 / 0.3e1 * t474) / 0.576e3))));

  t559 = t74 * params->B;
  t560 = t95 * t105;
  t577 = t381 * (-t560 + t128 * t130 * t29 * t117 / 0.32e2 + t128 * t137 * t29 * t139 / 0.576e3 - t127 * (0.2e1 / 0.3e1 * t155 - t119 * t29 * t117 / 0.4e1) / 0.8e1);
  t579 = t175 * params->B;
  t581 = t177 * t90;
  t582 = t85 * t182;
  t583 = 0.7e1 / 0.72e2 * t95;
  if(vrho != NULL && (p->info->flags & XC_FLAGS_HAVE_VXC))
    vsigma[0] = t198 * (-0.3e1 / 0.8e1 * t13 * t20 * (0.93333333333333333332e-3 * t23 * t11 * t29 * t39 - 0.93333333333333333332e-3 * t24 * t30 * t212 * (0.12600000000000000000e-1 / t31 * t33 * t35 + 0.12600000000000000000e-1 * t29 * t220)) + t559 * t577 + t166 * (t579 * t577 + t581 * t582 * t80 * (t583 + t185 * t111 * t29 * t117 / 0.576e3)));

  t595 = t381 * t560;
  if(vrho != NULL && (p->info->flags & XC_FLAGS_HAVE_VXC))
    vsigma[1] = t198 * (-0.2e1 * t559 * t595 + t166 * (-0.2e1 * t579 * t595 + 0.7e1 / 0.36e2 * params->A * t238 * params->B * t381 * t183));

  t643 = t381 * (-t560 + t128 * t130 * t56 * t122 / 0.32e2 + t128 * t137 * t56 * t142 / 0.576e3 - t127 * (0.2e1 / 0.3e1 * t151 - t114 * t56 * t122 / 0.4e1) / 0.8e1);
  if(vrho != NULL && (p->info->flags & XC_FLAGS_HAVE_VXC))
    vsigma[2] = t198 * (-0.3e1 / 0.8e1 * t13 * t51 * (0.93333333333333333332e-3 * t23 * t11 * t56 * t66 - 0.93333333333333333332e-3 * t24 * t57 * t430 * (0.12600000000000000000e-1 / t58 * t60 * t62 + 0.12600000000000000000e-1 * t56 * t438)) + t559 * t643 + t166 * (t579 * t643 + t581 * t582 * t80 * (t583 - t185 * t111 * t56 * t122 / 0.576e3)));

#ifndef XC_DONT_COMPILE_FXC

  if(order < 2) return;


#endif

#endif


}

