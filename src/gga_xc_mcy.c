/*
 Copyright (C) 2006-2007 M.A.L. Marques

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/


#include "util.h"

#define XC_GGA_XC_MCY1    911 /* "c" term in MCY1 hyper-gga functional */
#define XC_GGA_XC_MCY2    921 /* "c" term in MCY2 hyper-gga functional */

typedef struct{
  double A, B, c, d, lambda_p, scale_lambda_p;
} gga_xc_mcy_params;

void xc_gga_xc_mcy_init(xc_func_type *p)
{
  gga_xc_mcy_params *params;

  assert(p!=NULL && p->params == NULL);
  p->params = malloc(sizeof(gga_xc_mcy_params));
  params = (gga_xc_mcy_params *) (p->params);      

  /* values of constants in standard LYP functional */
  switch(p->info->number){
  case XC_GGA_XC_MCY1:
    /* default set by set_ext_params */
    params->lambda_p = 0.63;
    params->scale_lambda_p = 1.0;
    break;
  case XC_GGA_XC_MCY2:
    params->lambda_p = 0.69;
    params->scale_lambda_p = 0.9955;
    break;
  default:
    fprintf(stderr, "Internal error in gga_xc_mcy\n");
    exit(1);
  }
}

static const func_params_type ext_params[] = {
  {"_A", 0.04918, "Parameter A of LYP"},
  {"_B", 0.132,   "Parameter B of LYP"},
  {"_c", 0.2533,  "Parameter c of LYP"},
  {"_d", 0.349,   "Parameter d of LYP"},
};

static void 
set_ext_params(xc_func_type *p, const double *ext_params)
{
  gga_xc_mcy_params *params;

  assert(p != NULL && p->params != NULL);
  params = (gga_xc_mcy_params *) (p->params);

  params->A = get_ext_param(p->info->ext_params, ext_params, 0);
  params->B = get_ext_param(p->info->ext_params, ext_params, 1);
  params->c = get_ext_param(p->info->ext_params, ext_params, 2);
  params->d = get_ext_param(p->info->ext_params, ext_params, 3);
}

#include "maple2c/gga_exc/gga_xc_mcy.c"
#include "work_gga.c"

#ifdef __cplusplus
extern "C"
#endif
const xc_func_info_type xc_func_info_gga_xc_mcy1 = {
  XC_GGA_XC_MCY1,
  XC_EXCHANGE_CORRELATION,
  "MCY1",
  XC_FAMILY_GGA,
  {&xc_ref_MoriSanchez2006_091102, NULL, NULL, NULL, NULL},
  XC_FLAGS_3D | MAPLE2C_FLAGS,
  1e-32,
  4, ext_params, set_ext_params,
  xc_gga_xc_mcy_init, NULL,
  NULL, work_gga, NULL
};

#ifdef __cplusplus
extern "C"
#endif
const xc_func_info_type xc_func_info_gga_xc_mcy2 = {
  XC_GGA_XC_MCY2,
  XC_EXCHANGE_CORRELATION,
  "MCY2",
  XC_FAMILY_GGA,
  {&xc_ref_MoriSanchez2006_091102, NULL, NULL, NULL, NULL},
  XC_FLAGS_3D | MAPLE2C_FLAGS,
  1e-32,
  4, ext_params, set_ext_params,
  xc_gga_xc_mcy_init, NULL,
  NULL, work_gga, NULL
};

