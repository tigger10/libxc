/*
 Copyright (C) 2006-2007 M.A.L. Marques

 This Source Code Form is subject to the terms of the Mozilla Public
 License, v. 2.0. If a copy of the MPL was not distributed with this
 file, You can obtain one at http://mozilla.org/MPL/2.0/.
*/


#include "util.h"

#define XC_MGGA_C_MCY    912 /* "b" term in MCY1 hyper-gga functional */

typedef struct{
  double beta, d;
  double C0_c[4];
  double scale_b;
} mgga_c_mcy_params;

void mgga_c_mcy_init(xc_func_type *p)
{
  mgga_c_mcy_params *params;

  assert(p!=NULL && p->params == NULL);
  p->params = malloc(sizeof(mgga_c_mcy_params));
  params = (mgga_c_mcy_params *) (p->params);      

  /* values of constants in standard LYP functional */
  switch(p->info->number){
  case XC_MGGA_C_MCY:
    /* default set by set_ext_params */
    break;
  default:
    fprintf(stderr, "Internal error in mgga_c_mcy\n");
    exit(1);
  }
}

static const func_params_type ext_params[] = {
  {"_beta", 0.06672455060314922, "beta"},
  {"_d", 2.8, "d"},
  {"_C0_c0", 0.53, "C0_c[0]"},
  {"_C0_c1", 0.87, "C0_c[1]"},
  {"_C0_c2", 0.50, "C0_c[2]"},
  {"_C0_c3", 2.26, "C0_c[3]"},
  {"_scale_b", 4.0, "scale_b"}
};

static void 
set_ext_params(xc_func_type *p, const double *ext_params)
{
  mgga_c_mcy_params *params;

  assert(p != NULL && p->params != NULL);
  params = (mgga_c_mcy_params *) (p->params);

  params->beta    = get_ext_param(p->info->ext_params, ext_params, 0);
  params->d       = get_ext_param(p->info->ext_params, ext_params, 1);
  params->C0_c[0] = get_ext_param(p->info->ext_params, ext_params, 2);
  params->C0_c[1] = get_ext_param(p->info->ext_params, ext_params, 3);
  params->C0_c[2] = get_ext_param(p->info->ext_params, ext_params, 4);
  params->C0_c[3] = get_ext_param(p->info->ext_params, ext_params, 5);
  params->scale_b = get_ext_param(p->info->ext_params, ext_params, 6);
}

#include "maple2c/mgga_exc/mgga_c_mcy.c"
#include "work_mgga.c"

#ifdef __cplusplus
extern "C"
#endif
const xc_func_info_type xc_func_info_gga_xc_mcy1 = {
  XC_MGGA_C_MCY,
  XC_CORRELATION,
  "MCY",
  XC_FAMILY_MGGA,
  {&xc_ref_MoriSanchez2006_091102, NULL, NULL, NULL, NULL},
  XC_FLAGS_3D | MAPLE2C_FLAGS,
  1e-32,
  7, ext_params, set_ext_params,
  mgga_c_mcy_init, NULL,
  NULL, NULL, work_mgga
};

